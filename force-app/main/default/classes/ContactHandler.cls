//Handler class to insert logic for the trigger to use
Public with sharing class ContactHandler {
    
    public static void isBeforeInsert(List<Contact> newContacts){
 
        //looping through contacts in bulk
        for(Contact c: newContacts){
            
            //if a contact's email is empty, generate one based on the first name, last name, and current date & time
            if(String.isBlank(c.Email)){
                c.Email = c.FirstName + c.LastName.remove('\'').remove(' ') + String.valueOf(DateTime.now().format('dd-MM-yyyy-HH-mm-ss')) + '@noprovider.com';
            }

            //Calculating and inserting the contact's age based on their birthday
            Date dob = c.Birthdate;
            Integer age = Integer.valueOf(dob.daysBetween(Date.Today()))/365;

            c.Age__c = age;

        }
    }
}