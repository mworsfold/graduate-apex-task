@isTest
public class TestContactHandler {
    //positive test: I can insert a contact

    @isTest static void insertGoodContact() {
        Contact c = new Contact(FirstName='John', 
                                LastName='Smith', 
                                Email='j@smith.com', 
                                Birthdate=Date.parse('01/01/1979'),
                                Credit_Card_Type__c='visa');

        Test.startTest();
        Database.SaveResult result = Database.insert(c, false);
        Test.stopTest();
		
		//verifiying the test; displaying an error if the test failed
        System.assert(result.isSuccess());       
    }


    //negative test: generating a contact with invalid fields
    
    @isTest static void insertBadContact() {
        Contact c = new Contact(FirstName=' ', 
                                LastName=' ', 
                                Email=' ',  
                                Birthdate=Date.parse('01/01/1979'), 
                                Credit_Card_Type__c='amex');
        
        Test.startTest();
        Database.SaveResult result = Database.insert(c, false);
        Test.stopTest();
        
        //verifiying the test - displaying an error if the test failed
        //we want the test to pass if we cannot insert a contact with these details, hence !result.isSuccess()
        System.assert(!result.isSuccess());
    }

    //Test blank email insertion 

    @isTest static void insertGoodEmail() {
        //if email field is blank, generate one
        Contact c = new Contact(FirstName='John', 
                                LastName='O\'Neil', 
                                Email='', 
                                Birthdate=Date.parse('01/01/1979'),
                                Credit_Card_Type__c='visa');

        if(String.isBlank(c.Email)){
            c.Email = c.FirstName 
                    + c.LastName.remove('\'').remove(' ') 
                    + String.valueOf(DateTime.now().format('dd-MM-yyyy-HH-mm-ss')) 
                    +'@noprovider.com';
        }

        Test.startTest();
        Database.SaveResult result = Database.insert(c, false);
        Test.stopTest();

        System.assert(result.isSuccess());  
    }

    @isTest static void insertBadEmail() {
        Contact c = new Contact(FirstName=' ', 
                                LastName=' ', 
                                Email='', 
                                Birthdate=Date.parse('01/01/1979'),
                                Credit_Card_Type__c='amex');
                                
        if(String.isBlank(c.Email)){
            c.Email = c.FirstName 
                    + c.LastName.remove('\'').remove(' ') 
                    + String.valueOf(DateTime.now().format('dd-MM-yyyy-HH-mm-ss')) 
                    + '@noprovider.com';
        }

        Test.startTest();
        Database.SaveResult result = Database.insert(c, false);
        Test.stopTest();

        System.assert(!result.isSuccess());  
    }
}