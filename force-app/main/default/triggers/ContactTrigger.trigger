//bulk trigger to handle the insertion of large number of contacts
trigger ContactTrigger on Contact (before insert) {
    ContactHandler.isBeforeInsert(Trigger.new);
}